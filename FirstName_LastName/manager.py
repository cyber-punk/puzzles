# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from copy import deepcopy


class Task(object):

    def __init__(self, name, points=0):
        super(Task, self).__init__()
        self.name = name
        self.points = points

    def __repr__(self):
        return '{}: {}'.format(self.name, self.points)


class Employee(object):

    def __init__(self, name, max_points=None):
        super(Employee, self).__init__()
        self.name = name
        self.max_points = max_points or 0
        self._tasks = []  # no first sin
        self._total_points = 0

    def add_task(self, task):
        self._tasks.append(task)
        self._total_points += task.points

    @property
    def total_points(self):
        return self._total_points

    def is_free(self, task_points=0):
        return not self.max_points or self.total_points + task_points <= self.max_points

    def __repr__(self):
        return '{}: {} {}'.format(self.name, self.total_points, self._tasks)


class Manager(object):

    def __init__(self, employees=None, tasks=None):
        super(Manager, self).__init__()
        self.employees = employees and deepcopy(employees) or []
        self.tasks = tasks and deepcopy(tasks) or []
        self._unassigned_tasks = []
        self._unassigned_points = 0

    @staticmethod
    def _sort_employees(task_points=0):
        def sort_func(employee):
            if employee.is_free(task_points):
                return -1 * employee.total_points
        return sort_func

    def manage(self):
        self.tasks.sort(key=lambda item: -1 * item.points)
        while self.tasks:
            task = self.tasks.pop(0)
            employee = self.employees[-1]
            if not employee.is_free(task.points):
                self.mark_as_unassigned(task)
                continue
            employee.add_task(task)
            self.employees.sort(key=self._sort_employees(task.points))

    def mark_as_unassigned(self, task):
        self._unassigned_tasks.append(task)
        self._unassigned_points += task.points

    @property
    def unassigned_points(self):
        return self._unassigned_points

    def __repr__(self):
        result = 'Employees: {}'.format(self.employees)
        if self._unassigned_points:
            result += ' Unassigned tasks: {} {}'.format(self._unassigned_points,
                                                        self._unassigned_tasks)
        return result
