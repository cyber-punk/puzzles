# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest

from manager import Task, Employee, Manager


class TestUnlimited(unittest.TestCase):

    def setUp(self):
        self.employees = [
            Employee('Alisa'),
            Employee('Bob'),
            Employee('Carmen')
        ]

    def test_simple(self):
        tasks = [
            Task('A', 21),
            Task('B', 10), Task('C', 10),
            Task('D', 5), Task('E', 5), Task('F', 5), Task('G', 5)
        ]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 21)
        self.assertEqual(manager.employees[1].total_points, 20)
        self.assertEqual(manager.employees[2].total_points, 20)

    def test_fibonacci(self):
        tasks = [
            Task('A', 21),
            Task('B', 13),
            Task('C', 8),
            Task('D', 5),
            Task('E', 3),
            Task('F', 2),
            Task('G', 1)
        ]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 21)
        self.assertEqual(manager.employees[1].total_points, 16)
        self.assertEqual(manager.employees[2].total_points, 16)


class TestLimited(unittest.TestCase):

    def setUp(self):
        self.max_points = 15
        self.employees = [
            Employee('Alisa', max_points=self.max_points),
            Employee('Bob', max_points=self.max_points),
            Employee('Carmen', max_points=self.max_points),
            Employee('Diego', max_points=self.max_points)
        ]

    def test_insufficient_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(10)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 10)
        self.assertEqual(manager.employees[3].total_points, 10)
        self.assertEqual(manager.unassigned_points, 0)

    def test_enough_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(12)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 0)

    def test_over_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(13)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 5)

    def test_big_task_left(self):
        tasks = [Task('T{:02d}'.format(i + 1), 16 if i == 0 else 5) for i in range(13)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 16)

    def test_big_task_right(self):
        tasks = [Task('T{:02d}'.format(i + 1), 16 if i == 12 else 5) for i in range(13)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 16)

    def test_big_task_both_sides(self):
        tasks = [Task('T{:02d}'.format(i + 1), 16 if i in (0, 12) else 5) for i in range(13)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 10)
        self.assertEqual(manager.unassigned_points, 32)

    def test_big_task_inside(self):
        tasks = [Task('T{:02d}'.format(i + 1), 16 if i == 6 else 5) for i in range(13)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 15)
        self.assertEqual(manager.employees[1].total_points, 15)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 16)


class TestDifferentLimits(unittest.TestCase):

    def setUp(self):
        self.employees = [
            Employee('Alisa', max_points=5),
            Employee('Bob', max_points=10),
            Employee('Carmen', max_points=15),
            Employee('Diego', max_points=20)
        ]

    def test_insufficient_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(9)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 5)
        self.assertEqual(manager.employees[1].total_points, 10)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 15)
        self.assertEqual(manager.unassigned_points, 0)

    def test_enough_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(10)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 5)
        self.assertEqual(manager.employees[1].total_points, 10)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 20)
        self.assertEqual(manager.unassigned_points, 0)

    def test_over_points(self):
        tasks = [Task('T{:02d}'.format(i + 1), 5) for i in range(11)]
        manager = Manager(self.employees, tasks)
        manager.manage()
        print manager
        self.assertEqual(manager.employees[0].total_points, 5)
        self.assertEqual(manager.employees[1].total_points, 10)
        self.assertEqual(manager.employees[2].total_points, 15)
        self.assertEqual(manager.employees[3].total_points, 20)
        self.assertEqual(manager.unassigned_points, 5)
