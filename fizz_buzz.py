# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function

import gevent


def fizz_buzz_worker(worker_number, max_value):
    with open('worker_{}.log'.format(worker_number), 'w+') as f:
        for i in range(1, max_value+1):
            s = ''
            if i % 3 == 0:
                s += 'Fizz'
            if i % 5 == 0:
                s += 'Buzz'
            f.write(s or str(i))
    return 1


def do_fizz_buzz(workers_count=10, max_value=1000):
    jobs = [gevent.spawn(fizz_buzz_worker, i, max_value)
            for i in range(1, workers_count+1)]
    gevent.joinall(jobs)
    result = sum([job.value or 0 for job in jobs])
    return result == workers_count


if __name__ == '__main__':
    assert do_fizz_buzz()
    print('OK')
