#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function


"""
    Saddle point of the matrix is a point that 
    either is a local minimum in row
        and local maximum in column
    or is a local maximum in row
        and local minimum in column.
"""


class Matrix(object):
    _bold_font = '\033[1m\033[91m'
    _bold_end = '\033[0m'
    _data = None

    def __init__(self, data):
        super(Matrix, self).__init__()
        self._data = data
        self.width = len(data)
        self.height = data and len(data[0]) or 0

    def __getitem__( self, key):
        if isinstance(key, (int, long)):
            return self._data[key]
        i, j = key
        if i < 0 or i > self.width - 1 or \
           j < 0 or j > self.height - 1:
            return 0
        return self._data[i][j]

    def __str__(self):
        result = []
        for i in range(self.width):
            for j in range(self.height):
                if self._is_saddle_point(i, j):
                    s = '{}{:2d}{}'.format(
                        self._bold_font,
                        self[i, j],
                        self._bold_end
                    )
                else:
                    s = '{:2d}'.format(self[i, j])
                result.append(s)
                result.append(' ')
            result.append('\n')
        return ''.join(result)

    def _is_saddle_point(self, p, q):
        return (
            self[p, q - 1] > self[p, q] < self[p, q + 1] and
            self[p - 1, q] < self[p, q] > self[p + 1, q]
        ) or (
            self[p, q - 1] < self[p, q] > self[p, q + 1] and
            self[p - 1, q] > self[p, q] < self[p + 1, q]
        )

    @property
    def saddle_points(self):
        result = {}
        for i in range(self.width):
            for j in range(self.height):
                if self._is_saddle_point(i, j):
                    result[(i, j)] = self[i, j]
        return result


if __name__ == '__main__':
    m = Matrix((
        (0, 1, 9, 3),
        (7, 5, 8, 3),
        (9, 2, 9, 4),
        (4, 6, 7, 1)
    ))
    print('Saddle Points')
    print('-' * 13)
    print(m)
    saddle_points = m.saddle_points
    assert len(saddle_points) == 2
    assert saddle_points[1, 1] == 5
    assert saddle_points[1, 2] == 8
